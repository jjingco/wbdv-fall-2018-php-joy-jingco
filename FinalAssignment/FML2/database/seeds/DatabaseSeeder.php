<?php

// author: Joy Jingco
// Date: 11/3/2018

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Profile;
use App\Models\Story;
use App\Models\Nearly_Story;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $user1=new User();
        $user1->email ='fake1@email.com';
        $user1->name ='Captain Barkley';
        $user1->password =bcrypt('12341234');
        $user1->save();

        $user2=new User();
        $user2->email ='fake2@email.com';
        $user2->name = 'Sir Whiskers';
        $user2->password =bcrypt('12341235');
        $user2->save();

        $user3=new User();
        $user3->email ='fake3@email.com';
        $user3->name = 'Madamme Boots';
        $user3->password =bcrypt('12341236');
        $user3->save();

        $user4=new User();
        $user4->email ='fake4@email.com';
        $user4->name = 'Doctor Fuzzy';
        $user4->password =bcrypt('12341237');
        $user4->save();

        $user5=new User();
        $user5->email ='fake5@email.com';
        $user5->name = 'Miss Fluffy';
        $user5->password =bcrypt('12341238');
        $user5->save();



        $userProfile1 =new Profile ();
        $userProfile1->user_id=$user1 ->id;
        $userProfile1->gender="male";
        $userProfile1->handle="cbarkley";
        $userProfile1->age="10";
        $userProfile1->status="widowed";
        $userProfile1->email ='fake1@email.com';
        $userProfile1->save();

        $userProfile2 =new Profile ();
        $userProfile2->user_id=$user2 ->id;
        $userProfile2->gender="male";
        $userProfile2->handle="swhiskers";
        $userProfile2->age="5";
        $userProfile2->status="married";
        $userProfile2->email ='fake2@email.com';
        $userProfile2->save();

        $userProfile3 =new Profile ();
        $userProfile3->user_id=$user3 ->id;
        $userProfile3->gender="female";
        $userProfile3->handle="mdmBoots";
        $userProfile3->age="7";
        $userProfile3->status="open";
        $userProfile3->email ='fake3@email.com';
        $userProfile3->save();

        $userProfile4 =new Profile ();
        $userProfile4->user_id=$user4 ->id;
        $userProfile4->gender="male";
        $userProfile4->handle="dfuzz";
        $userProfile4->age="8";
        $userProfile4->status="single";
        $userProfile4->email ='fake4@email.com';
        $userProfile4->save();

        $userProfile5 =new Profile ();
        $userProfile5->user_id=$user5 ->id;
        $userProfile5->gender="female";
        $userProfile5->handle="msfluff";
        $userProfile5->age="9";
        $userProfile5->status="single";
        $userProfile5->email ='fake5@email.com';
        $userProfile5->save();



        $nearlyStory1 =new Nearly_Story();
        $nearlyStory1->user_id=$user1 ->id;
        $nearlyStory1->nearly_story='Today I woke up and thought I was late for work -
                            I ran around trying to find my shirt. I got mad
                            at my wife thinking she misplaced it as she usually 
                            tries to put my stuff away without telling me. I
                            eventually decided to just wear the first shirt I saw.
                            Half way through the door I realized I got laid off
                            last week and she left. FML.';
        $nearlyStory1->save();

        $nearlyStory2 =new Nearly_Story();
        $nearlyStory2->user_id=$user2 ->id;
        $nearlyStory2->nearly_story='Today I decided to start working on a project for
                            school. After numerous attempts of downloading the software
                            I started to get frusterated. Then I realized that 
                            the file just keeps failing. I had to finish it at school. FML.';
        $nearlyStory2->save();

        $nearlyStory3 =new Nearly_Story();
        $nearlyStory3->user_id=$user3 ->id;
        $nearlyStory3->nearly_story='I opened the cupboard and realized I have nothing in 
                            my pantry for food. I guess I forgot to go grocery shopping. FML.';
        $nearlyStory3->save();

        $nearlyStory4 =new Nearly_Story();
        $nearlyStory4->user_id=$user4 ->id;
        $nearlyStory4->nearly_story='I went in for a job interview and found out that the 
                            position was already filled. The lady told me she tried to call
                            but no one answered. I realized I had been so neverous 
                            I forgot my phone at home. FML.';
        $nearlyStory4->save();
        
        $nearlyStory5 =new Nearly_Story();
        $nearlyStory5->user_id=$user5 ->id;
        $nearlyStory5->nearly_story='My husband spent weeks telling me about a restaurant he wanted to go to.
                            Yesterday, we finally went and I got food poisoning. FML.';
        $nearlyStory5->save();




        $mainStory1 =new Story();
        $mainStory1->user_id=$user1 ->id;
        $mainStory1->story="Today, I got a ticket for doing 38mph in a 45mph zone. 
                        Apparently you can be cited for 'impeding traffic' even 
                        when you're the only car on the road. FML";
        $mainStory1->save();

        $mainStory2 =new Story();
        $mainStory2->user_id=$user1 ->id;
        $mainStory2->story="Today, I was turned down for a job because they thought 
                        I lied about having a six-month internship. Why? The secretary 
                        I told off for stealing my lunch every single day told them she 
                        hadn't heard of me, and the new company won't listen. FML";
        $mainStory2->save();

        $mainStory3 =new Story();
        $mainStory3->user_id=$user1 ->id;
        $mainStory3->story=" Today, I found out that my 6-year-old son was named after my 
                        wife's 'Favorite Uncle' because he might also be the father. FML";
        $mainStory3->save();

        $mainStory4 =new Story();
        $mainStory4->user_id=$user1 ->id;
        $mainStory4->story=" Today, I tried to get my midterm project done. I installed all necessary 
                        software and even restarted my computer a bunch of times, but I still didn't 
                        get anywhere. I then googled as to why my command prompt ket erring out and
                        when I thought I finally found a solution, the webpage it was on also errored out.
                        my project is due in a couple of hours and I still can't get my Sass to work. FML";
        $mainStory4->save();



        $mainStory5 =new Story();
        $mainStory5->user_id=$user2 ->id;
        $mainStory5->story="Today, I got a ticket for doing 38mph in a 45mph zone. 
                        Apparently you can be cited for 'impeding traffic' even 
                        when you're the only car on the road. FML";
        $mainStory5->save();

        $mainStory6 =new Story();
        $mainStory6->user_id=$user2 ->id;
        $mainStory6->story="Today, I was turned down for a job because they thought 
                        I lied about having a six-month internship. Why? The secretary 
                        I told off for stealing my lunch every single day told them she 
                        hadn't heard of me, and the new company won't listen. FML";
        $mainStory6->save();

        $mainStory7 =new Story();
        $mainStory7->user_id=$user2 ->id;
        $mainStory7->story=" Today, I found out that my 6-year-old son was named after my 
                        wife's 'Favorite Uncle' because he might also be the father. FML";
        $mainStory7->save();



        $mainStory8 =new Story();
        $mainStory8->user_id=$user3 ->id;
        $mainStory8->story="Today, I got a ticket for doing 38mph in a 45mph zone. 
                        Apparently you can be cited for 'impeding traffic' even 
                        when you're the only car on the road. FML";
        $mainStory8->save();

        $mainStory9 =new Story();
        $mainStory9->user_id=$user3 ->id;
        $mainStory9->story="Today, I was turned down for a job because they thought 
                        I lied about having a six-month internship. Why? The secretary 
                        I told off for stealing my lunch every single day told them she 
                        hadn't heard of me, and the new company won't listen. FML";
        $mainStory9->save();

        $mainStory10 =new Story();
        $mainStory10->user_id=$user3 ->id;
        $mainStory10->story=" Today, I found out that my 6-year-old son was named after my 
                        wife's 'Favorite Uncle' because he might also be the father. FML";
        $mainStory10->save();



        $mainStory11 =new Story();
        $mainStory11->user_id=$user4 ->id;
        $mainStory11->story="Today, I got a ticket for doing 38mph in a 45mph zone. 
                        Apparently you can be cited for 'impeding traffic' even 
                        when you're the only car on the road. FML";
        $mainStory11->save();

        $mainStory12 =new Story();
        $mainStory12->user_id=$user4 ->id;
        $mainStory12->story="Today, I was turned down for a job because they thought 
                        I lied about having a six-month internship. Why? The secretary 
                        I told off for stealing my lunch every single day told them she 
                        hadn't heard of me, and the new company won't listen. FML";
        $mainStory12->save();


        $mainStory13 =new Story();
        $mainStory13->user_id=$user5 ->id;
        $mainStory13->story="Today, I got a ticket for doing 38mph in a 45mph zone. 
                        Apparently you can be cited for 'impeding traffic' even 
                        when you're the only car on the road. FML";
        $mainStory13->save();

        $mainStory14 =new Story();
        $mainStory14->user_id=$user5 ->id;
        $mainStory14->story="Today, I was turned down for a job because they thought 
                        I lied about having a six-month internship. Why? The secretary 
                        I told off for stealing my lunch every single day told them she 
                        hadn't heard of me, and the new company won't listen. FML";
        $mainStory14->save();

        $mainStory15 =new Story();
        $mainStory15->user_id=$user5 ->id;
        $mainStory15->story=" Today, I found out that my 6-year-old son was named after my 
                        wife's 'Favorite Uncle' because he might also be the father. FML";
        $mainStory15->save();



        foreach (User::all() as $user) {
            foreach (Story::all() as $mainStory) {
                $rand = rand(1,100);
                if($rand < 70) {
                    $user ->agreeVotes() ->attach($mainStory);

                }
            }
        }


    }
}
