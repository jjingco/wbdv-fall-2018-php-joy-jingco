@extends ('layout')

@section ('content')

<div class="user-submit container">
    <h1>Submit an FML:</h1>
    <form action="/FMLSubmit" method="post">
        <?php echo csrf_field() ?>
        <div class="form-group">
            <textarea name="story" 
                        id="story" 
                        cols="30" 
                        rows="15"
                        class="form-control"
            >
            </textarea>
            <?php if($err = $errors->first('story')): ?>
                    <span class="text-danger"><?php echo $err?>
                    </span>
            <?php endif; ?>
            <input class='form-control submit-button mt-3' 
                    type="submit" 
                    name='submit' 
                    class="btn btn-primary"
            >

        </div>
    </form>
</div>


@endsection