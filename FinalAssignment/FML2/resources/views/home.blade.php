@extends('layout')

@section('content')

    <main class=" container ">
        <div class="row">
            <div class="main-feed">

                <div class="tertiary-navigation row dflex align-content-left">
                    <div class="tertiary-option hilarious"> 
                        <img height="30px" data-cfsrc="/images/smiley/hilarious.png" src="https://www.fmylife.com/images/smiley/hilarious.png"> 
                        Funniest FMLs of the Day
                    </div>
                    <div class="tertiary-option">
                        <img height="30px" data-cfsrc="/images/smiley/weird.png" src="https://www.fmylife.com/images/smiley/weird.png">
                        Weirdest FMLs of the Day
                    </div>
                    <div class="tertiary-option">
                        <i class="fas fa-comment"></i> 
                        Most Commented FMLs of the Day
                    </div>
                </div>

                <div class="news-feed"> 
                    <?php foreach($mainfeed as $story):?>
                        @include ('mainfeed')
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="side-feed">
                <div class="Nearly-FML">
                    <div class="title1">
                        <div>Nearly FML</div>
                    </div>
                    <?php foreach($nearlys as $nearlyStory):?>
                        @include ('nearly')
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </main>




@endsection
