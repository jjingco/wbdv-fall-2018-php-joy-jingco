<!DOCTYPE html>
<html lang="en" dir='ltr'>

<!-- author: Joy Jingco
Date: 11/3/2018 -->


<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
    <title>FML Example</title>
</head>
<body>
    <header>
        <div class="navigation-pane">
            <div class="container">
                <div class="row dflex align-content-left">
                    <div class="logo">
                            <img height="70px"  src="https://www.fmylife.com/images/logos/fml/logo_fmylife.png" >
                        </div>
                        <div class="menu-option">
                            <div>Moderate the FMLs</div>
                        </div>
                        <div class="menu-option">
                            <div>Even more FMLs</div>
                        </div>
                        <div class="menu-option">
                            <?php if(Auth:: check()): ?>
                                <a href="/FMLSubmit">Submit FML!</a>
                            <?php endif; ?>
                        </div>
                        <div class="menu-option">
                            <input type="text" placeholder=' Search' id="searchBox">
                            <span class="search-icon">
                                <i class="fas fa-search"></i>
                            </span>
                        </div>
                        <div class="menu-option">
                            <?php if(Auth:: check()): ?>
                                Hi <?php echo request()->user()->name?> !
                                <a href="/profile">Profile</a>
                                    @include('logout')
                                
                            <?php else: ?>
                                <a href="/login">Login</a>
                            <?php endif; ?>
                        </div>
                        

                    </div>
                </div>
            </div>



        <div class="secondary-navigation ">
            <div class="container">
                <div class=" row dflex align-content-left">
                    <div class="home-icon">
                        <a href="/home"><i class="fas fa-home"></i></a>
                    </div>
                    <div class="secondary-option"> 
                        FML NEWS
                        <i class="far fa-file-alt"></i>
                    </div>
                    <div class="secondary-option">
                        RANDOM
                        <i class="fas fa-dice-five"></i>
                    </div>
                    <div class="secondary-option">
                        <div>SPICY </div>
                    </div>
                    <div class="secondary-option"> 
                        THE TOP 
                        <i class="fas fa-trophy"></i>
                    </div>
                    <div class="secondary-option">
                        BLOG 
                        <i class="fas fa-coffee"></i>
                    </div>
                    <div class="secondary-option">
                        MEDICS 
                        <i class="fas fa-plus"></i>
                    </div>
                    <div class="secondary-option">
                        <div>SCHOOL </div>
                    </div>
                    <div class="secondary-option">
                        <div>MISCELLANEOUS </div>
                    </div>
                    <div class="secondary-option">
                        <div>GEEK </div>
                    </div>
                    <div class="secondary-option">
                        <div>HOLIDAYS </div>
                    </div>
                    <div class="secondary-option">
                        <div>TRANSPORATION </div>
                    </div>
                </div>
            </div>

            
        </div>
    </header>
    <main>
        @yield('content')
    </main>
</body>
</html>
    