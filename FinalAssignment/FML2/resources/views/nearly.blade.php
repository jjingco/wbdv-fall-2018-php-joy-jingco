<p class="comment">
    <?php echo $nearlyStory->nearly_story?>
</p>
<?php if(Auth:: check()): ?>
    <div class="reader-votes row"> 
        <div class="agreeVotes row">
            <input type="button" class="agree-btn" value="I agree, your life sucks">
            <div class="agree-votes">
                <?php echo rand(1,100)?>
            </div>
        </div>
        <div class="disagreeVotes row">
            <input type="button" class="disagree-btn-nearly" value="You deserved it">
            <div class="disagree-votes-nearly">
                <?php echo rand(1,100)?>
            </div>
        </div>
    </div>
<?php endif; ?>
    
<div class="authors">
    <a href="<?php echo $nearlyStory->user->id ?>" >
    By: <?php echo $nearlyStory->user->name?>
    / <?php echo $nearlyStory->user->profile->handle?>
    / <?php echo $nearlyStory->user->created_at->format('M j');?>
    </a>
</div>
<hr>