@extends ('layout')

@section ('content')

<div class="user-profile container">
    <h1>Edit Your Profile:</h1>
    <form action="/profile" method="post">
        <?php echo csrf_field() ?>
        <div class="form-group">
            <input class='form-control' 
                    type="text" 
                    name='gender' 
                    placeholder='Gender'
                    value='<?php echo $profile->gender?>'
                />
                <?php if($err = $errors->first('gender')): ?>
                        <span class="text-danger"><?php echo $err?>
                        </span>
                <?php endif; ?>
        </div>
        <div class="form-group">
            <input class='form-control' 
                    type="text" 
                    name='handle'
                    placeholder='Handle'
                    value='<?php echo $profile->handle?>'
                />
                <?php if($err = $errors->first('handle')): ?>
                        <span class="text-danger"><?php echo $err?>
                        </span>
                <?php endif; ?>
        </div>
            <input class='form-control' 
                    type="text" 
                    name='email' 
                    placeholder='Email'
                    value='<?php echo $profile->email?>'
                />
                <?php if($err = $errors->first('email')): ?>
                        <span class="text-danger"><?php echo $err?>
                        </span>
                <?php endif; ?>

        <div class="form-group">
            <input class='form-control' 
                    type="text" 
                    name='age' 
                    placeholder='Age'
                    value='<?php echo $profile->age?>'
                />
                <?php if($err = $errors->first('age')): ?>
                        <span class="text-danger"><?php echo $err?>
                        </span>
                <?php endif; ?>
        </div>
            <input class='form-control' 
                    type="text" 
                    name='status' 
                    placeholder='Relationship Status'
                    value='<?php echo $profile->status?>'
                />
                <?php if($err = $errors->first('status')): ?>
                        <span class="text-danger"><?php echo $err?>
                        </span>
                <?php endif; ?>
        <div class="form-group">
            <input class='form-control submit-button mt-3' 
                    type="submit" 
                    name='submit' 
                    class="btn btn-primary"
            />

        </div>
    </form>
</div>


@endsection