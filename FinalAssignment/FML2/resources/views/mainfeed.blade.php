<div class="social-media-all row dflex align-content-left">
    <div class="small-logo">
        <div>FML</div>
    </div>
    <div class="social-media-icons row">
        <div class="twitter social-media">
            <i class="fab fa-twitter"></i>
        </div>
        <div class="facebook social-media">
            <i class="fab fa-facebook-f"></i>
        </div>
        <div class="save-story social-media">
            <i class="fas fa-star"></i>
        </div>
    </div>
</div>
<div class="story">
    <p>
        <?php echo $story->story?>
    </p>
</div>
<?php if(Auth:: check()): ?>
    <div class="reader-votes row dflex align-content-left"> 
        <div class="agree row dflex align-content-left">
            <input type="button" class="agree-btn" value="I agree, your life sucks">
            <div class="agree-votes">
                <?php echo $story->agreeVotes->count()?>
            </div>
        </div>
        <div class="disagree row dflex align-content-left">
            <input type="button" class="disagree-btn" value="You deserved it">
            <div class="disagree-votes">
                <?php echo rand(1,100)?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="bottom-pane">
    <div class="emoji-pane row dflex align-content-left">
        <div class="no-comment emoji">
            <img height="30px" data-cfsrc="/images/smiley/amusing.png" src="https://www.fmylife.com/images/smiley/amusing.png">
            <div class="dialogue-box">
                <?php echo rand(1,150)?>
            </div>
        </div>
        <div class="happy emoji">
            <img height="30px" data-cfsrc="/images/smiley/funny.png" src="https://www.fmylife.com/images/smiley/funny.png">
            <div class="dialogue-box">
                <?php echo rand(1,150)?>
            </div>
        </div>
        <div class="shocked emoji">
            <img height="30px" data-cfsrc="/images/smiley/weird.png" src="https://www.fmylife.com/images/smiley/weird.png">
            <div class="dialogue-box">
                <?php echo rand(1,150)?>
            </div>
        </div>
        <div class="lol emoji">
            <img height="30px" data-cfsrc="/images/smiley/hilarious.png" src="https://www.fmylife.com/images/smiley/hilarious.png">
            <div class="dialogue-box">
                <?php echo rand(1,150)?>
            </div>
        </div>
    </div> 
    <div class="author-pane">
        Author: <?php echo $story->user->name?>
        / <?php echo $story->user->profile->handle?>
        / <?php echo $story->user->profile->gender?>
        / <?php echo $story->created_at->format('M j');?>
    </div>           
</div>
<hr>