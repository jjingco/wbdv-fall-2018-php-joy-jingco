<?php

// author: Joy Jingco
// date 10/30/2018

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Story;
use App\Models\Nearly_Story;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allStories = $this ->getAllStories();
        $allNearlySection = $this->getAllNearlySection();
        
        $viewData = [
            'mainfeed' => $allStories,
            'nearlys' => $allNearlySection,
        ]; 
        
        return view('home', $viewData);

    }

    public function getAllStories()
    {
        $allStories = Story::orderBy('created_at','desc')->get();

        return $allStories;
    }


    public function getAllNearlySection()
    {
        
        $allNearlySection = Nearly_Story::orderBy('created_at','desc')->get();

        return $allNearlySection;
    
    }
    
}
