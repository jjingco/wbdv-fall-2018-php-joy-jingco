<?php

// author: Joy Jingco
// Date: 10/30/2018

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Story;
use App\Models\Nearly_Story;
use App\Models\Profile;
use App\User;


class FMLController extends Controller
{
    

    
    public function index($id=1)
    {
        $primaryUser = $this ->getPrimaryUser($id);
        $mainStories= $this ->getMainStories($primaryUser);
        $nearlys = $this ->getNearlySection($primaryUser);
    
    
        
        $viewData = 
        [
            'user' => $primaryUser,
            'mainfeed' => $mainStories,
            'nearlys' => $nearlys,
        ];

        return view('welcome', $viewData);

    }



        public function getPrimaryUser($id)
        {
            $primaryUser = User::findOrFail($id);

            return $primaryUser;

        }


        public function getMainStories($primaryUser)
        {
            $mainStories = Story::where('user_id', $primaryUser->id)->get();

            return $mainStories;



        }


        public function getNearlySection($primaryUser)
        {
            
            $nearlys = Nearly_Story::where('id','!=', $primaryUser->id)->limit(5)->get();

            return $nearlys;
      
        }
            
       
    
    

}