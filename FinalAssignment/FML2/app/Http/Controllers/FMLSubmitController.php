<?php
// author: Joy Jingco
// Date: 10/30/2018


namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Story;
use App\User;

class FMLSubmitController extends Controller
{
    public function index()
    {
        
        $user = Auth::user();
        
        $mainStory = $user->story;
        
        $viewData = [
            'user' => $user,
            'mainStory' => $mainStory,
        ];

        return view('FMLSubmit_form', $viewData);
    }

    public function update()
    {
        
        $user = Auth::user();
        
        $formData = request()->all();

        request()->validate(
        [
            'story' => 'required|max:255',
        ]);

        $mainStory = new Story;
        $mainStory->user_id = $user->id;
        $mainStory->story = $formData['story'];

        $mainStory->save();

        
        return redirect("/home");
    }
}