<?php

// author: Joy Jingco
// Date: 11/5/2018


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\User;

class ProfileController extends Controller
{
    public function index()
    {
        
        $user = request()->user();
        
        $profile = $user->profile;
        
        $viewData = [
            'user' => $user,
            'profile' => $profile,
        ];

        return view('profile_form', $viewData);
    }

    public function update()
    {
        
        $user = request()->user();

       
        $formData = request()->all();
        
        request()->validate(
        [
            'gender' => 'required|max:10',
            'handle' => 'unique:user_profiles,handle',
            'age' => 'required',
            'status' => 'required',
        ]);
        
        $profile = $user->profile;
        
        $profile->gender = $formData['gender'];
        $profile->handle = $formData['handle'];
        $profile->email = $formData['email'];
        $profile->age = $formData['age'];
        $profile->status = $formData['status'];
        $profile->save();

        
        
        
        
        


        return redirect("/home");
    }
}
