<?php
// author: Joy Jingco
// date: 10/30/2018


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    public $dates = ['created_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
        // one to one relationship
    }
    public function agreeVotes()
    {
        return $this->belongsToMany('App\User');
        // one to many relationship
    }
    

}