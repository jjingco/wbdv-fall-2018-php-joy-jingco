<?php

// author: Joy Jingco
// Date: 11/3/2018

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nearly_Story extends Model
{
    public $dates = ['created_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
        // one to one relationship
    }

}
