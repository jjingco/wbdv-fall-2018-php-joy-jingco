<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// author: Joy Jingco
// date: 10/30/2018

Route::get('/', 'FMLController@index');
Route::get('/contact', 'ContactController@index');


Route::get('/profile', 'ProfileController@index');
Route::post('/profile', 'ProfileController@update');

Route::get('/FMLSubmit', 'FMLSubmitController@index');
Route::post('/FMLSubmit', 'FMLSubmitController@update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{id}', 'FMLController@index');
