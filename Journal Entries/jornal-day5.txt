DATE: 10/29/2018

1. What challenges you had, and how you overcame them
	
	Over the weekend I downloaded: Node JS, Composer and Laravel, but despite
	restarting my computer several times and even re-downloading the software
	I kept getting the same error - Sass didn't recognize the webpack that was
	pre-installed by laravel. I tried googling the answer and I tried several
	methods including manually moving the file around, but it still didn't work.
	
	In light of this, I decided to use my twitter document as a template.
	It worked but I can only concur there was something internally wrong
	with the file. 


2. If you weren't challenged, what you did to push yourself
	
	 Life was just filled with lots of challenges today. :(

	
3. What did you do outside of class

	I used stockoverflow alot to learn how to navigate around laravel.
	The hardest part of this whole process was definitely learning how 
	to troubleshoot given the errors. 