DATE: 10/29/2018

1. What challenges you had, and how you overcame them
	
	My laravel wasn't connecting to my SQL database. I first realized
	it wasn't connecting because it was missing a foreign key as
	pointed out by my prof. But even by adding the User-Id column,
	i found it was still crashing with the message -object not found. 
	
	I then switched all of my column headings from capitalized to 
	uncapitalized and it seemed to fix the problem. 


2. If you weren't challenged, what you did to push yourself
	
	 Life was just filled with lots of challenges today. :(

	
3. What did you do outside of class

	I read the SQL textbook to get a better understanding of
	relational databases and how they connect. When I first came 
	across one it was through access and we connected the tables
	manually, but in php you connect the tables through a function 
	code.