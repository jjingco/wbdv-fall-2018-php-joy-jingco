<pre>
<?php


$list = [
    'a','b','c'
];

// preferred way: foreach loop
foreach ($list as $value) {
    echo $value."\n";
}

// old school for loop 
for ($i = 0; $i< count($list); $i++) {
    echo $list[$i]."\n";
}



$bob = [
    'name' => 'Bob',
    'age' => 21
];
echo "\n";
foreach ($bob as $key => $value) {
    echo "$key: $value \n";
}

$jane = [
    'name' => 'Jane',
    'age' => 24
];

$people = [$bob,$jane];

echo "\n";
foreach ($people as $value) {
    foreach ($value as $k => $v) {
        echo "$k: $v\n";
    }
}

echo "\n";
foreach ($people as $person) {
    echo $person['name']. ' is '
    . $person['age'].' years old.'
    ."\n";
}