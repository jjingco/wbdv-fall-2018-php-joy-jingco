<pre>
<?php

//$array = [];    //empty array
//echo $array;    //arrays cannot be turned into strings so they can't be echoed

// numberically indexed arrays:
$array = ['a','b','c',TRUE,1,'d'];
var_dump($array);    //only var_dump for debugging 
echo "\n";   //needs to be double quotes --> only 
echo $array[0]."\n";    
echo $array[1]."\n";    
echo $array[2];

//append elements to array using array_push
array_push($array,'e','f');

echo "\n"; 
var_dump($array); 
echo "\n"; 

  
 $array[9] = 'q';
 echo "\n"; 
 var_dump($array); 
 echo "\n"; 
 
// explicitly et 'q' to end of array
$array[count($array)] = 'q';
//implicitly set 'r' to end of array
$array[] ='r';

var_dump($array); 
echo "\n"; 

// to check whether or not a set exists
if(isset($array[222])) {
    echo $array[222];
} else {
    echo '222 not there';
}

// can also check your variables --> if they exist or not
if(isset($var)) {
    echo $var;
} else {
    echo 'var not there';
}

// you can overwrite element
$array[2] = 'overwrite!!!';
echo "\n"; 
var_dump($array); 
echo "\n"; 

echo "sorted array:";
sort($array);
var_dump($array); 
echo "\n"; 

echo " custom sorted array:";
function cmp ($a,$b) {
    $a <=> $b;
}
usort($array,'compare');
var_dump($array); 
echo "\n"; 







 


?>
</pre>