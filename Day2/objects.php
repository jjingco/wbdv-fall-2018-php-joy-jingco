<pre>
<?php

// PRO TIPS:
// atl + up and down keys makes the whole line move
// ctr C anywhere on a line copies the whole line


// $bob = [
//     'name' => 'Bob',
//     'age' => 21
// ];
// $bobObject = (object) $bob;
// var_dump($bobObject);

// echo "\n";


// // cake example: class = recipe of cake, cake = instance
// class Cake{
//     public $type = 'chocolate';
//     private $secretIngredient = 'ginger';

//     // when a function is attached to a class = "method"
//     public function describeTaste() {
//         return 'Yummy!';
//     }
// }
// $cake = new Cake();
// echo $cake -> describeTaste(); //won't work without echo
// // in JS it's like cake.describeTaste()
// echo "\n";
// var_dump($cake);

// echo "\n";
// echo $cake->type; //type is a property, not a variable

// // echo "\n";
// // echo $cake -> secretIngredient; //not going to work since not public

// echo "\n";
// // can overwrite the type:
// echo $cake->type = 'vanilla';



// ************************ NEW TOPIC - CONSTRUCTOR **************************

// // THIS
// // only exists in the class definition --> calls itself
// class Cake {
//     public $type;
//     private $secretIngredient;

//     public function __construct($type,$secretIngredient)
//     {
//         $this->type = $type;
//         $this->secretIngredient = $secretIngredient;
//     }

//     public function describeTaste() {
//         return 'Yummy!'.$this->type.'!';
//     }
//     public function recipe() {
//         return $this->secretIngredient;
//     }
// }

// $carrotCake = new Cake('carrot','sludge'); //carrot becomes the type in construtor
// echo $carrotCake ->describeTaste().'<br/>';
// echo $carrotCake ->recipe().'<br/>';

// $chocoCake = new Cake('chocolate','ginger');
// echo $chocoCake ->describeTaste().'<br/>';
// echo $chocoCake ->recipe().'<br/>';



// ************************ NEW TOPIC - INHERITANCE **************************


class Cake {
    public $type;
    public function describe() {
        return "I'm a $this->type cake.";
    }
}

class ChocolateCake extends Cake {
    public $type = 'Chocolate';  //overrides the type as "chocolate"
}

$cake = new Cake ();
echo $cake ->describe ();
echo '<br/>';
$chocoCake = new ChocolateCake ();
echo $chocoCake->describe();

class DatabaseTable {
    public function save() {}
    public function fetch() {}
}

class CakeTable extends DatabaseTable {
    public $name = 'cakes';
}

$myTable = new CakeTable();
$myTable->save('data');