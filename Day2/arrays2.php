<pre>
<?php

// associateive arrays (a thing with properties NOT A LIST --> that would be numeric):
// it's more like an object

$bob = [
    'name' => 'Bob',
    'age' => 21
];

var_dump($bob);
echo "\n";
//use pretty print (built in php) to make it look more presentable
echo json_encode($bob, JSON_PRETTY_PRINT); 

$bob['hairColor'] = 'brown';
echo json_encode($bob, JSON_PRETTY_PRINT); 

if (!isset($bob['notThere'])) {
    echo 'NOT THERE';
}