<?php

$a = 1;
$b = 2.4;
$c = true;
$d = "something";
$e = [1,2.4,'foo'];
$f = new stdClass();
$g = function () {
    return 'Hi';
};
// echo $a;
// to figure out the type:
var_dump($a,$b,$c,$d,$e, $f, $g);