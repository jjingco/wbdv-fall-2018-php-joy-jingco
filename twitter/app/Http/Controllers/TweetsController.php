<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;

class User {}

class TweetsController extends Controller
{
    public function index()
    {
        $mrBean = $this ->getMrBean();

        $user1 = $this->getUser1();
        $user2 = $this->getUser2();
        $user3 = $this->getUser3();
        $user4 = $this->getUser4();

        $tweets = $this->getTweets($mrBean);

        $viewData = [
            'user' => $mrBean,
            'youMightLike' => [$user1,$user2,$user3,$user4],
            'tweetExamples' => $tweets,
        ]; 

        return view('page1', $viewData);
    }

    public function getMrBean () 
    {
        $mrBean = new User();

        $mrBean ->profilePic = "https://pbs.twimg.com/profile_images/521655203011899392/pxOndDc7_400x400.png";
        $mrBean ->name = 'Mr. Bean';
        $mrBean ->twitterName = '@MrBean';
        $mrBean ->productionCompany = 'Tiger Aspect Productions';
        $mrBean ->website = 'mrbean.com';
        $mrBean ->joined = 'Joined June 2010';
        $mrBean ->NumberTweets = '2,154';
        $mrBean ->NumberFollowing = '53';
        $mrBean ->NumberFollowers = '172K';
        $mrBean ->NumberLikes = '402';
        $mrBean ->NumberMoments = '2';

        return $mrBean;
    }

    public function getUser1() 
    {
        $user1 = new User();
        $user1 ->image = "https://pbs.twimg.com/profile_images/719427633117671425/J5wKAQlQ_bigger.jpg"; 
        $user1 ->name = 'Abhishek Bachchan';
        $user1 ->handle = '@juniorbachchan';

        return $user1;
    }
    public function getUser2() 
    {
        $user2 = new User();
        $user2 ->image = "https://pbs.twimg.com/profile_images/917431263551565824/fCDyXoLk_bigger.jpg"; 
        $user2 ->name = 'Sanjay Dutt';
        $user2 ->handle = '@duttsanjay';

        return $user2;
    }
    public function getUser3() 
    {
        $user3 = new User();
        $user3 ->image = "https://pbs.twimg.com/profile_images/1052210317302718465/ewdLh-yS_bigger.jpg"; 
        $user3 ->name = 'Tarek Fatah';
        $user3 ->handle = '@tarekfatah';

        return $user3;
    }
    public function getUser4() 
    {
        $user4 = new User();
        $user4 ->image = "https://pbs.twimg.com/profile_images/1053789673607364614/nFsQUsGO_bigger.jpg"; 
        $user4 ->name = 'Yellow Verse';
        $user4 ->handle = '@Shane_Eagle';

        return $user4;
    }

    public function getTweets($mrBean)
    {

        $tweet1 = new User();
        $tweet1 ->beanImage  = "https://pbs.twimg.com/profile_images/521655203011899392/pxOndDc7_400x400.png";
        $tweet1 ->user = $mrBean;
        $tweet1 ->user = $mrBean;
        $tweet1 ->tweetDate = "Oct 25";
        $tweet1 ->description = "<video preload='none'  poster='https://pbs.twimg.com/media/DpuYXOLXgAEFyLm.jpg' src='lob:https://twitter.com/68e5bf16-2fd9-4f9c-9022-f4b52a53d89a' style='width: 80%; height: 100%; background-color: black; transform: rotate(0deg) ;'></video>";

        $tweet2 = new User();
        $tweet2 ->beanImage  = "https://pbs.twimg.com/profile_images/521655203011899392/pxOndDc7_400x400.png";
        $tweet2 ->user = $mrBean;
        $tweet2 ->user = $mrBean;
        $tweet2 ->tweetDate = "Oct 25";
        $tweet2 ->description = "<video preload='none'  poster='https://pbs.twimg.com/media/DpuYXOLXgAEFyLm.jpg' src='lob:https://twitter.com/68e5bf16-2fd9-4f9c-9022-f4b52a53d89a' style='width: 80%; height: 100%; background-color: black; transform: rotate(0deg) ;'></video>";
      
        return [$tweet1,$tweet2];
    }

}