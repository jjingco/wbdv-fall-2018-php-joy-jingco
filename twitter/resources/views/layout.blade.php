<!DOCTYPE html>
<html lang="en" dir='ltr'>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
    <title>Document</title>
</head>
<body>
    <header class="flex flex-v">
        <div class="top-menu d-flex align-items-left">
            <div class="home"><i class="fab fa-twitter"></i> Home</div>
            <div class="moments"><i class="fas fa-bolt"></i> Moments</div>
        </div>
        <div class="hero-row">
        </div>
        <div class="stats">
            <div class="flex flex-h justify-content-center">
                <div class="stat">
                    <div class="label d-flex align-items-center">
                       <div class="stat-sections">
                           <div class="tweets">Tweets</div>
                            <div class="number-tweets">
                                <?php echo $user->NumberTweets ?>
                            </div>
                       </div> 
                       <div class="stat-sections">
                           <div class="following">Following</div>
                            <div class="number-following">
                                <?php echo $user->NumberFollowing ?>
                            </div>
                       </div> 
                       <div class="stat-sections">
                           <div class="followers">Followers</div>
                            <div class="number-followers">
                                <?php echo $user->NumberFollowers ?>    
                            </div>
                       </div> 
                       <div class="stat-sections">
                           <div class="likes">Likes</div>
                           <div class="number-likes">
                                <?php echo $user->NumberLikes ?>
                           </div>
                       </div>
                       <div class="stat-sections">
                           <div class="moments">Moments</div>
                            <div class="moments-likes">
                                <?php echo $user->NumberMoments ?>    
                            </div>
                       </div>
                       <form>
                           <input type="button" value="Follow" class="button">
                       </form> 
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo $user->profilePic ?>" 
            class="circular profile-pic" 
            style="width: 200px">
    </header>
    <main class="flex full-page container">
    </main>
    



</body>
</html>
