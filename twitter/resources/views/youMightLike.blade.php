<div class="d-flex align-items-left">
    <img src="<?php echo $tweeter->image ?>" 
        class="circular"
        style="width:80px"
        >
    <div class="tweeters"> 
        <div class="name">
            <?php echo $tweeter->name?>
        </div> 
        <?php echo $tweeter->handle?>
    </div> 
</div>