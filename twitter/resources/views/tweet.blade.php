<div class="userTweetDetails d-flex align-items-left">
    <div class="bean-Image">
        <img src="<?php echo $tweet->beanImage ?>" 
                class="circular"
                style="width:70px"
            >
    </div>
    <div class="tweetInfo d-flex align-items-left">
        <div class="bean-Name">
            <?php echo $tweet->user->name ?>
        </div>
        <div class="bean-Handle">
            <?php echo $tweet->user->twitterName ?>
        </div>
        <div class="tweet-Date">
            <?php echo $tweet->tweetDate?>
        </div>
    </div>
</div>


<div class="description-Tweet">
    <?php echo $tweet ->description ?>
</div>