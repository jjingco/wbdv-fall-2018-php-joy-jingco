<?php

// // these "INCLUDES" are dependencies, without them you can't run the code
// // in a similar way the Staff class depends on Person so it's okay to not have it in this
// // spreadsheet but you need to include Person in Staff

include_once './Shop/Cake.php';
include_once './Shop/Staff.php';
include_once './Shop/DeliveryTruck.php';
include_once './Shop/Shop.php';
include_once './Shop/Equipment.php';
include_once './Shop/Oven.php';
include_once './Shop/HealthInspector.php';
include_once './School/Student.php';



// NAMESPACE EXAMPLE: 

use Shop\Cake as Cake;
use Shop\Staff as Staff;
use Shop\HealthInspector as HealthInspector;
use Shop\Equipment as Equipment;
use Shop\Oven as Oven;
use Shop\DeliveryTruck as DeliveryTruck;
use Shop\Shop as Shop;

use School\Student as Student; //namespace single reference*** 
use School\Person as Person; //namespace single reference*** 


function main() {

    echo '<h1> The Cake Example </h1>';

    $cake = new Cake();
    $bob = new Staff();
    echo $bob ->eat($cake);
    echo '<br/>';

    $bob = new HealthInspector();
    echo $bob ->inspect($cake);
    echo '<br/>';

    $rock = new Equipment();
    $oven = new Oven ();
    echo $oven ->bake($rock);
    echo '<br/>';

    $delivery = new DeliveryTruck();
    echo $delivery ->drive($cake);
    echo '<br/>';

    $shop = new Shop();
    echo $shop ->bake($cake);

    echo '<br/>';
    echo '<br/>';
    echo '<h1> The School Example </h1>';
    // $jill = new \School\Student(); //includes the namespacing OR:
    $jill = new Student();  //use the reference from above***
    $person = new Person();
    echo $jill ->intro($person);
    echo '<br/>';

}

echo main();